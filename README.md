# README

See LICENSE file for code and data licensing information.

This repository contains code written in [`julia`](https://julialang.org/) v1.7 to simulate the model nano turbine dynamics described in "Sustained unidirectional rotation of a self-organized DNA rotor on a nanopore" by Xin Shi, Anna-Katharina Pumm, Jonas Isensee, Wenxuan Zhao, Daniel Verschueren, Alejandro Martin-Gonzalez, Ramin Golestanian, Hendrik Dietz, and Cees Dekker. ([link](https://doi.org/10.1038/s41567-022-01683-z))

You can either download a zipped package from the [gitlab page](https://gitlab.gwdg.de/LMP-pub/nanoturbines)
of the repository or clone the code from the command line via
```bash
git clone https://gitlab.gwdg.de/LMP-pub/nanoturbines.git
```
into a directory of your choice.

To get started, ensure that `julia` is installed on your system. Navigate to this repository and start a `julia` REPL.

```julia
import Pkg
Pkg.activate(".")
Pkg.instantiate()
```

An example simulation is contained in `simulationscript.jl`, which can be executed via

```julia
include("simulationscript.jl")
```

It produces a file containing a list of rod configurations at different time points.
