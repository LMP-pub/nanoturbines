using DrWatson
@quickactivate
using StaticArrays
using Symbolics
using LinearAlgebra
using JLD2
using DelimitedFiles
using NearestNeighbors

###########################################################################################
# Load numerical electric + flow fields and return functions that can be evaluated
###########################################################################################

const field_fun = let
    pot_file = joinpath(dirname(@__FILE__),"E_50mMNaCl_withoutDNA.txt")
    # A matrix with columns r, z, U
    # absorb units into field strength
    data = readdlm(pot_file; comments=true, comment_char='%' )
    @. data = data * SA[1/25 1/25 0 8.53e-4 0 8.53e-4]
    kdtree = KDTree(data[:,1:2]')
    function getpoint(x,y)
        idxs, dists = nn(kdtree, SA[x,y])
        i = idxs[1]
        data[i, 4], data[i, 6]
    end
end


const flow_fun = let
    flow_file = joinpath(dirname(@__FILE__),"FlowV_50mMNaCl_withoutDNA.txt")
    # A matrix with columns r, z, |v|, v_r, v_z
    data = readdlm(flow_file; comments=true, comment_char='%' )
    # rescale data 25nm → 1
    # absorb units into flow velocity
    @. data = data * [1/25 1/25 40 40 40]
    kdtree = KDTree(data[:,1:2]')
    function getpoint(x,y)
        idxs, dists = nn(kdtree, SA[x,y])
        i = idxs[1]
        data[i, 4], data[i, 5]
    end
end

###########################################################################################
## Use Symbolics.jl to write down the linear algebra for bending elasticity
###########################################################################################
# Relevant formulas in
# http://www.cs.columbia.edu/cg/pdfs/143-rods.pdf
# on pages 4, 7 (eq 1 + pg 7 "special case of isotropic rod")

@variables eⁱ[1:3] eⁱ⁻¹[1:3]
@variables l

# Curvature times binormal vector at ith segment
κ𝐛ᵢ = 2 * (eⁱ⁻¹ × eⁱ) / ( l^2 + eⁱ⁻¹ ⋅ eⁱ )

crossmatrix(e) = [0 -e[3] e[2];
                  e[3] 0 -e[1];
                 -e[2] e[1] 0]

# Verification
@assert (crossmatrix(eⁱ) * eⁱ⁻¹) - (eⁱ × eⁱ⁻¹) |> collect |> iszero


# collect in the below expression is only necessary due to some missing methods
# for symbolic arrays

∇ᵢ₋₁κ𝐛ᵢκ𝐛ᵢ = (2 * crossmatrix(eⁱ) +  collect(κ𝐛ᵢ * eⁱ') )' * κ𝐛ᵢ / (l^2 + eⁱ⁻¹⋅eⁱ)
∇ᵢ₊₁κ𝐛ᵢκ𝐛ᵢ = (2 * crossmatrix(eⁱ⁻¹) - collect(κ𝐛ᵢ * eⁱ⁻¹') )' * κ𝐛ᵢ / (l^2 + eⁱ⁻¹⋅eⁱ)

# symbolic simplification + compile executable functions
term = simplify(∇ᵢ₋₁κ𝐛ᵢκ𝐛ᵢ; expand=true)
term_expr = build_function(term, [eⁱ⁻¹; eⁱ; l])
eval(:(∇ᵢ₋₁κ𝐛ᵢκ𝐛ᵢ_fun = $(term_expr[2])))

term = simplify(∇ᵢ₊₁κ𝐛ᵢκ𝐛ᵢ; expand=true)
term_expr = build_function(term, [eⁱ⁻¹; eⁱ; l])
eval(:(∇ᵢ₊₁κ𝐛ᵢκ𝐛ᵢ_fun = $(term_expr[2])))

###########################################################################################
## Define equations of motion
###########################################################################################

eom = (du, u, p) -> begin
    # variables
    (; l, N, e, α) = p
    # buffers
    (; force, tmp, tmp2, inputvec) = p
    # friction coefficients
    Kₙ = p.Kₙ
    Kₜ = Kₙ / p.Kₙ╱Kₜ

    @views @. e = u[2:end, :] - u[1:end-1, :]
    force .= 0.0
    inputvec[7] = p.l

    # Compute bending forces
    for j = 2:p.N-1
        inputvec[1:3] .= @view e[j-1, :]
        inputvec[4:6] .= @view e[j, :]

        ∇ᵢ₋₁κ𝐛ᵢκ𝐛ᵢ_fun(tmp, inputvec)

        @. force[j-1,:] -= 2α/l*tmp
        @. force[j,:]   += 2α/l*tmp

        ∇ᵢ₊₁κ𝐛ᵢκ𝐛ᵢ_fun(tmp, inputvec)

        @. force[j,:]   += 2α/l*tmp
        @. force[j+1,:] -= 2α/l*tmp
    end

    # Stretching of rod
    for j = 1:p.N-1
        truel = norm(@view e[j, :])
        t1 = e[j,1]
        t2 = e[j,2]
        t3 = e[j,3]

        T1 = t1 / sqrt(t1^2 + t2^2 + t3^2)
        T2 = t2 / sqrt(t1^2 + t2^2 + t3^2)
        T3 = t3 / sqrt(t1^2 + t2^2 + t3^2)

        force[j, 1] += -p.k*(l-truel)/l * T1
        force[j, 2] += -p.k*(l-truel)/l * T2
        force[j, 3] += -p.k*(l-truel)/l * T3
        force[j+1, 1] += +p.k*(l-truel)/l * T1
        force[j+1, 2] += +p.k*(l-truel)/l * T2
        force[j+1, 3] += +p.k*(l-truel)/l * T3
    end

    # Add repelling force near the membrane.
    for j = 2:p.N-1
        x = (u[j,1] + u[j-1,1]) / 2
        y = (u[j,2] + u[j-1,2]) / 2
        z = (u[j,3] + u[j-1,3]) / 2
        r = sqrt(x^2 + y^2)
        # Additional term to strengthen surface
        if r ≥ 0.8 && 0.5 < z < 0.6
            force[j, 3]  += p.surface*p.l*(atan(20*(r -0.9) )/π + 0.5)*(0.6-z)
            force[j-1, 3] += p.surface*p.l*(atan(20*(r -0.9) )/π + 0.5)*(0.6-z)
        end
    end
    
    # Electrophoretic + Flow contributions
    for j = 2:p.N
        # flow field
        x = (u[j,1] + u[j-1,1]) / 2
        y = (u[j,2] + u[j-1,2]) / 2
        z = (u[j,3] + u[j-1,3]) / 2
        r = sqrt(x^2 + y^2)
        e_r, e_z = field_fun(r, z)::Tuple{Float64, Float64}
        v_r, v_z = flow_fun(r, z)::Tuple{Float64, Float64}

        force[j, 1] += -p.field*x/r * l * e_r /2
        force[j, 2] += -p.field*y/r * l * e_r /2
        force[j, 3] += -p.field     * l * e_z /2
        force[j-1, 1] += -p.field*x/r * l * e_r /2
        force[j-1, 2] += -p.field*y/r * l * e_r /2
        force[j-1, 3] += -p.field     * l * e_z /2

        # flow vec
        vel = SA[(p.flow *x/r * v_r),
                 (p.flow *y/r * v_r ),
                 (p.flow *      v_z )]

        # tangent vector
        tmp .= @view e[j-1, :]
        T = tmp
        T .= T ./norm(T)

        # force
        tmp2 .= Kₜ .* (vel⋅T) .*T .+ Kₙ .* (vel .- (vel⋅T) .*T)
        @. force[j, :] +=  tmp2 /2 * l
        @. force[j-1, :]+= tmp2 /2 * l
    end

    ## decompose according to mobility
    ## Mobility may be reduced near the membrane
    function μfactor(r,z)
        if r ≥ 0.9 && z < 0.7
            return p.dragfactor
        end
        1.0
    end

    for j = 2:p.N-1
        r = sqrt(u[j,1]^2 + u[j,2]^2)
        z = u[j,3]
        μf = μfactor(r,z)

        t1 = e[j,1] + e[j-1, 1]
        t2 = e[j,2] + e[j-1, 2]
        t3 = e[j,3] + e[j-1, 3]

        T1 = t1 / sqrt(t1^2 + t2^2 + t3^2)
        T2 = t2 / sqrt(t1^2 + t2^2 + t3^2)
        T3 = t3 / sqrt(t1^2 + t2^2 + t3^2)

        @views tangentforce = force[j, 1:3] ⋅ SA[T1,T2,T3]
        du[j, 1] = μf*1/(Kₜ*p.l) * tangentforce * T1
        du[j, 2] = μf*1/(Kₜ*p.l) * tangentforce * T2
        du[j, 3] = μf*1/(Kₜ*p.l) * tangentforce * T3

        du[j, 1] += μf*1/(Kₙ*p.l) * (force[j,1] - tangentforce * T1)
        du[j, 2] += μf*1/(Kₙ*p.l) * (force[j,2] - tangentforce * T2)
        du[j, 3] += μf*1/(Kₙ*p.l) * (force[j,3] - tangentforce * T3)
    end
    # Special cases for end points
    # Tangent is equal to only neighboring tangent
    T1 = e[1,1] / p.l
    T2 = e[1,2] / p.l
    T3 = e[1,3] / p.l

    @views tangentforce = force[1, 1:3] ⋅ SA[T1,T2,T3]
    du[1, 1] = 1/(Kₜ*p.l) * tangentforce * T1
    du[1, 2] = 1/(Kₜ*p.l) * tangentforce * T2
    du[1, 3] = 1/(Kₜ*p.l) * tangentforce * T3

    du[1, 1] += 1/(Kₙ*p.l) * (force[1,1] - tangentforce * T1)
    du[1, 2] += 1/(Kₙ*p.l) * (force[1,2] - tangentforce * T2)
    du[1, 3] += 1/(Kₙ*p.l) * (force[1,3] - tangentforce * T3)

    T1 = e[N-1,1] / p.l
    T2 = e[N-1,2] / p.l
    T3 = e[N-1,3] / p.l

    @views tangentforce = force[N, 1:3] ⋅ SA[T1,T2,T3]
    du[N, 1] = 1/(Kₜ*p.l) * tangentforce * T1
    du[N, 2] = 1/(Kₜ*p.l) * tangentforce * T2
    du[N, 3] = 1/(Kₜ*p.l) * tangentforce * T3

    du[N, 1] += 1/(Kₙ*p.l) * (force[N,1] - tangentforce * T1)
    du[N, 2] += 1/(Kₙ*p.l) * (force[N,2] - tangentforce * T2)
    du[N, 3] += 1/(Kₙ*p.l) * (force[N,3] - tangentforce * T3)
    return nothing
end

###########################################################################################
## Run a simulation given initial conditions and parameter container
###########################################################################################

function _simulate(x0, states, p, tmax = 1000.0, t=0.0, framenum=0)
    du = zeros(size(x0))
    x = copy(x0)
    n = 0
    while (t < tmax && n < 5*10^8)
        eom(du, x, p)
        m = maximum(abs, du)
        dt = min(0.01, 1/(2000*m))
        @. x = x + dt*du

        if floor((t+dt)) > floor(t)
            println("Step: $n, time=$(round(t, digits=2)), dt=$(round(dt, digits=5)), zmin=$(round(minimum(x[:,3]),digits=4)), φ=$(round(angle(x[1,1]+im*x[1,2]), digits=4)), E2E=$(round(norm(x[1,:].-x[end,:]), digits=4))")
            flush(stdout)
            states[string(framenum,pad=5)] = copy(x)
            framenum += 1

            if minimum(x[:,3]) < -0.5
                #Assume that the thing must have translocated
                return x,t,framenum
            end
        end
        n+=1
        t += dt

    end
    x, t, framenum
end


###########################################################################################
## Run and store a simulation given (setup) parameters and output path
###########################################################################################

function simulate(params, outputfile)
    N = params[:N]
    L = params[:L]
    l = L / (N-1)
    x0 = zeros(N,3)
    x0[:, 1] .= range(-L/2, L/2; length=N)
    x0[:, 3] .= 0.6
    x0[:, 1] .+= params[:x0]
    x0[:, 2] .+= params[:y0]


    p = (;
        params...,
        l = l,
        e = zeros(N-1, 3),
        force = zeros(N, 3),
        inputvec = zeros(7),
        tmp = zeros(3),
        tmp2 = zeros(3)
    )

    states = Dict{String, Any}()
    try
        xsol, t, framenum = _simulate(x0, states, p, p.tmax)
    catch e
        println(e)
    end

    jldopen(outputfile, "w") do f
        g = JLD2.Group(f, "params")
        for (k,v) in pairs(p)
            k ∈ [:tmp, :tmp2, :inputvec, :force] && continue
            g[string(k)] = v
        end
        for (k,v) in pairs(states)
            f["states/"*k] = v
        end
    end
end


params = Dict(
    :tmax => 200.0,
    :N => 51,
    :L => 18.0,
    :α => 60,
    :field => 0.26,
    :flow => 19.5,
    :Kₙ => 11.85,
    :Kₙ╱Kₜ => 2.0,
    :surface => 5e4,
    :dragfactor => 1.0,
    :k => 2500.0,
    :x0 => 2.0,
    :y0 => 2.0,
)

simulate(params, "example.jld2")



Δt = 1
statesvec = jldopen("example.jld2", "r") do f
    sg = f["states"]
    map(1:Δt:length(keys(sg))) do n
        x = sg[string(n-1, pad=5)]
        Point3f0.(x)
    end
end;